package com.example.ryan.deathclockapp;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;


public class InputView extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_view);
/*
        NumberPicker picker = (NumberPicker) findViewById(R.id.genderView);
        picker.setMinValue(0);
        picker.setMaxValue(1);
        picker.setDisplayedValues( new String[] { "Male", "Female"} );
*/
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.advanced_view);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            boolean flag = false;
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(InputView.this, R.anim.on_click));
                ImageView image = (ImageView) findViewById(R.id.imageView);
                if (!flag) {
                    image.setImageResource(R.drawable.ic_action_name2);
                    flag = true;
                }
                else {
                    image.setImageResource(R.drawable.ic_action_name);
                    flag = false;
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_input_view, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
